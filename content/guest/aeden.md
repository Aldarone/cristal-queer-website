+++
Date = 2021-11-29T23:26:51+01:00
title = "Æden"
Pronouns = "Ael"
Type = "guest"
Instagram = "aeden_petitfeu"
Thumbnail = "img/guests/aeden.jpg"
+++

Æden est un catboi polyamoureux et pédé-gouin, malheureusement
souvent confondu avec un chien. Æl exprimera un cri capable de
briser un œuf si on lui demande ses pronoms.
