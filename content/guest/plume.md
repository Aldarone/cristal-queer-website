+++
Date = 2021-11-29T23:26:51+01:00
title = "Plume"
Pronouns = ""
Type = "guest"
Instagram = "evadserves"
Thumbnail = "img/guests/plume.jpg"
+++

Plume se plait à ne pas avoir de genre de prédilection, ni dans l'écriture ni dans la vie, préférant jouer sur la zone floue où ils se rencontrent. Sur [son blog](https://evadserves.ovh/), il est question de son rapport à l’écriture et au monde, de ses lectures et réflexions politiques, sont aussi partagés quelques textes de fiction et projets photo/peinture
