+++
Date = 2021-11-29T23:26:47+01:00
title = "Ram"
Pronouns = "Il"
Type = "host"
Thumbnail = "img/hosts/ram.webp"
+++

Ram n'aime pas les réseaux sociaux alors ici c'est le seul endroit où vous le
verrez sur internet.
