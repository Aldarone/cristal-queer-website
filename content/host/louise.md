+++
Date = 2021-11-29T23:21:46+01:00
title = "Lou Fleur-de-Sel"
Pronouns = "Æl"
Type = "host"
Instagram = "lou_de_sel"
Thumbnail = "img/hosts/louise.png"
+++

Lou Fleur-de-Sel est un⋅e transactiviste bisexuel⋅le, auteurice et photographe française résidant actuellement en Alsace. En 2018 æl fonde l'Amicale Radicale des Cafés Trans de Strasbourg avec son mari. Depuis fin 2019 æl est également læ responsable administrative du centre LGBTI+ de la ville de Strasbourg.
